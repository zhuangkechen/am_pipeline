import argparse
from pt_darts.asha_worker import AshaWorker
from cluster_lib import Master

args = None

@Master.register()
def main():
    master = Master(args)
    m = AshaWorker()
    m.run()


if __name__ == "__main__":
    main()
