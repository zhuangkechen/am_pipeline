#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import os
from .distributed_worker import DistributedWorker

class ModelTrainWorker(DistributedWorker):
    def __init__(self):
        # print("ModelTrainWorker init")
        super(ModelTrainWorker, self).__init__()

    def run(self, *args, **kwargs):
        raise NotImplementedError()
