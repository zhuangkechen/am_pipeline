#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import os
import abc

class Generator(object, metaclass=abc.ABCMeta):
    def __init__(self):
        return

    @abc.abstractmethod
    def run(self, *args, **kwargs):
        return
