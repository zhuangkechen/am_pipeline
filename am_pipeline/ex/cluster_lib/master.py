#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import os

class DaskEnv(object):
    def __init__(self, args, master_path):
        self.is_master = False
        self.__master_path__ = master_path

        return

    def start(self):
        self._install_dask()
        self._start_dask()
        if self.is_master:
            self._wait_worker()
        print("Dask Sever Start Success!")
        print("Dask Sever Start Success!")
        return

    def _install_dask(self):
        return

    def _start_dask(self):
        my_env = os.environ.copy()
        if 'PYTHONPATH' in my_env:
            my_env['PYTHONPATH'] = "{0}:{1}".format(my_env['PYTHONPATH'],
                                                    self.__master_path__)
        else:
            my_env['PYTHONPATH'] = self.__master_path__

        if self.is_master:
            subprocess.call(['nohup', 'dask-server', self.port], env=my_env)
        else:
            subprocess.call(['nohup', 'dask-server', self.port], env=my_env)

        return

    def _wait_worker(self):
        return


class Master(object):
    __master_path__ = None

    @classmethod
    def register(cls,*args):
        def decorator(fn):
            file_name = inspect.getfile(fn)
            file_path = os.path.abspath(file_name)
            module_path = os.path.dirname(file_path)
            cls.__master_path__ = module_path
            return fn
        return decorator

    def __init__(self, args):
        self.dask_env = DaskEnv(args, self.__master_path__)
        self.dask_env.start()
        if not self.dask_env.is_master:
            sys.exit(0)
        return

    def start(self):
        return


    def run(self, worker, config_dict):
        print("run")
        print(self.__master_path__)
        return
