#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import os
import abc
import subprocess

class DistributedWorker(object, metaclass=abc.ABCMeta):
    __worker_path__ = None
    __worker_module__ = None
    __worker_name__ = None

    @classmethod
    def register(cls,*args):
        def decorator(fn):
            file_name = inspect.getfile(cls)
            file_path = os.path.abspath(file_name)
            module_path = os.path.dirname(file_path)
            cls.__worker_path__ = module_path
            cls.__worker_module__ = os.path.basename(file_name)
            cls.__worker_name__ = cls.__name__
            return fn
        return decorator

    def __init__(self):
        # print("DisttrbutedWorker init")
        return

    def __call__(self, *args, **kwargs):
        my_env = os.environ.copy()
        if 'PYTHONPATH' in my_env:
            my_env['PYTHONPATH'] = "{0}:{1}".format(my_env['PYTHONPATH'],
                                                    self.__worker_path__)
        else:
            my_env['PYTHONPATH'] = self.__worker_path__
        cmd = "from {0} import {1};augment={2}();".format(
                                            self.__worker_module__,
                                            self.__worker_name__,
                                            self.__worker_name__)
        params = ""
        for arg in args:
            params = params + "{0},".format(arg)
        for k, v in kwargs:
            params = params + "{0}={1},".format(k, v)
        cmd = cmd + "augment.run({0})".format(params)
        subprocess.call(['python', '-c', cmd], env=my_env)
        return

    @abc.abstractmethod
    def run(self, *args, **kwargs):
        return
